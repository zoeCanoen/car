# TP1 CAR

## Créer un serveur FTP

### Auteurs

* Anthony Slimani
* Zoé Canoen

### Configuration du serveur FTP

Dans le répertoire `src/main/resources` se trouve un fichier **app.properties**. Ce fichier contient le port de connection, le répertoire racine du serveur FTP, ainsi que le level de log. Avant de lancer le serveur, il vous suffit de modifier ce fichier de configuration.

### Commandes

```
$ mvn javadoc:javadoc
$ mvn clean install
$ java -jar target/car.tp1-0.0.1-SNAPSHOT.jar
```

### Connexion

* Ouvrir Fillezilla
* Hôte : 127.0.0.1
* Identifiant : canoen
* Mot de passe : canoen
* port : 8080 (modifiable avec le **app.properties**)

### Travail effectué

| Code |         Description                                              | A faire | En cours | Validé |
| :--: |:---------------------------------------------------------------- | :-----: | :------: | :----: |
| CMP  | Le code du server compile correctement avec Maven                                        | | |X|
| DOC  | Le code du server est documenté (Readme.md, Javadoc)                                     | | |X|
| TST  | Le code du server est proprement testé (tests unitaires sous Junit)                      |X| | |
| EXE  | Le code du server est conçu en suivant les principes de conception objet                 | | |X|
| CON  | Je peux me connecter au serveur avec un client FTP                                       | | |X|
| BADU | Le serveur rejette ma connection si mon utilisateur est inconnu                          | | |X|
| BADP | Le serveur rejette ma connection si mon mot de passe est incorrect                       | | |X|
| LST  | Je peux lister le contenu d'un répertoire distant                                        | | |X|
| CWD  | Je peux entrer dans un répertoire distant                                                | | |X|
| CWD  | Je peux quitter un répertoire distant                                                    | | |X|
| ROOT | Je ne peux pas descendre dans la hiérachie au delà du répertoire racine de l'utilisateur | |X| |
| GETT | Je peux télécharger un fichier texte                                                     | |X| |
| GETB | Je peux télécharger un fichier binaire (image)                                           | |X| |
| GETR | Je peux télécharger un répertoire complet                                                | |X| |
| PUTT | Je peux mettre en ligne un fichier texte                                                 | |X| |
| PUTB | Je peux mettre en ligne un fichier binaire (image)                                       | |X| |
| PUTR | Je peux mettre en ligne un répertoire complet                                            | |X| |
| RENF | Je peux renommer un fichier distant                                                      | | |X|
| MKD  | Je peux créer un répertoire distant                                                      | | |X|
| REND | Je peux renommer un répertoire distant                                                   | | |X|
| RMD  | Je peux supprimer un répertoire distant                                                  | | |X|
| CLOS | Je peux couper proprement la connection (dans crasher le serveur)                        | | |X|
| PORT | Je peux configurer le port du serveur FTP                                                | | |X|
| HOME | Je peux configurer le répertoire racine du serveur FTP                                   | | |X|
| ACPA | Le serveur supporte le mode ACTIF et PASSIF                                              | | |X|
| THRE | Le serveur supporte la connexion de plusieurs clients simultanés                         |X| | |
| LOG  | Le code du serveur est fourni avec un logger                                             | | |X|

### Problèmes rencontrés

1. Par manque de temps nous n'avons pas testé nos classes car la majorité des classes utilisent des sockets et nous ne savons pas comment les tester.
2. Lors de la lecture d'un dossier "Nouveau Dossier", nous avons des soucis il y a un espace. Globalement, notre FTP gère très mal les espaces.
3. Le STOR ne s'execute qu'apres une fermeture et une reouverture du serveur FTP.
4. RETR s'est executé quelques fois en fermant et rouvrant le serveur FTP et Fillezilla.

### Remarques

1. Tous les tests ont été effectué à la main.
2. Nous avons rajouté des Logs.

### Composition du  tp

* Le package `car.tp1` contient le main
* Le package `car.tp1.core` contient les classes principales
* Le package `car.tp1.process` contient tous les process qui executent les commandes de l'utilisateur
* Le package `car.tp1.utils` contient les classes de log et de lecture des properties
* Le package `car.tp1.exception` contient les exceptions
