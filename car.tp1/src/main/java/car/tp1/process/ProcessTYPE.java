package car.tp1.process;

/**
 * Cette classe permet de sauvegarder le type du fichier
 * @author Zoe Canoen et Anthony Slimani
 *
 */
import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;
import car.tp1.core.FTPReply;
import car.tp1.utils.Logger;

public class ProcessTYPE implements ProcessRequest {

    public static final String PROCESS_NAME = "TYPE";

    public static final Logger LOGGER = new Logger(ProcessTYPE.class.getName());

    @Override
    /**
     * sauvegarder le type du fichier
     */
    public boolean process(FTPArguments arguments, FTPClient client) {
        FTPReply reply = FTPReply.REQUEST_501;
        String firstArgument = arguments.getFirstArguments();
        LOGGER.info("TYPE " + firstArgument);
        if (arguments.onlyOneArgument()) {
            switch (firstArgument) {
                case "A":
                    reply = FTPReply.REQUEST_200;
                    break;
                case "E":
                    reply = FTPReply.REQUEST_200;
                    break;
                case "I":
                    reply = FTPReply.REQUEST_200;
                    break;
                case "L":
                    reply = FTPReply.REQUEST_200;
                    break;
                default:
                    reply = FTPReply.REQUEST_504;
                    break;
            }
        }

        client.send(reply);
        return true;
    }
}
