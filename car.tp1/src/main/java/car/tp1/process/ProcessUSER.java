package car.tp1.process;

import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;
import car.tp1.core.FTPDatabase;
import car.tp1.core.FTPReply;
import car.tp1.utils.Logger;

/**
 * Cette classe permet de verifier l'utilisateur
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class ProcessUSER implements ProcessRequest {

    public static final String PROCESS_NAME = "USER";

    public static final Logger LOGGER = new Logger(ProcessUSER.class.getName());

    @Override
    /**
     * verifier l'utilisateur
     */
    public boolean process(FTPArguments arguments, FTPClient client) {
        boolean connection = false;
        FTPReply reply = FTPReply.REQUEST_501;

        String username = arguments.getFirstArguments();
        if (arguments.onlyOneArgument()) {
            if (arguments.onlyOneArgument() && FTPDatabase.INSTANCE.usernameExist(username)) {
                client.setUsername(username);
                reply = FTPReply.REQUEST_331;
                connection = true;
                LOGGER.info("Welcome " + username);
            } else {
                reply = FTPReply.REQUEST_430;
                LOGGER.info("Unknown user " + username);
            }
        }
        client.send(reply);
        return connection;
    }
}