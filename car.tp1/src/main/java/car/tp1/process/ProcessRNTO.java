package car.tp1.process;

import java.io.IOException;

import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;
import car.tp1.core.FTPCommand;
import car.tp1.core.FTPReply;
import car.tp1.utils.Logger;

/**
 * Cette classe permet de renommer un fichier
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class ProcessRNTO implements ProcessRequest {

    public static final String PROCESS_NAME = "RNTO";

    public static final Logger LOGGER = new Logger(ProcessRNTO.class.getName());
	
    @Override
    /**
     * renommer un fichier
     */
    public boolean process(FTPArguments arguments, FTPClient client) {
        FTPReply reply = FTPReply.REQUEST_501;

        if (arguments.isNotEmpty()) {
            String fileToRename = client.getFileToRename();
            if (fileToRename == null) {
                reply = FTPReply.REQUEST_503;
            } else {
                try {
                    String currentPath = client.getCurrentPath() + "/";
                    String pathOldFile = currentPath + client.getFileToRename();
                    String pathNewFile = currentPath + arguments.getFirstArguments();
                    LOGGER.info(pathOldFile + FTPCommand.SEPARATOR_COMMAND + pathNewFile);
                    String cmdRename = "mv " + pathOldFile + " " + pathNewFile;
                    Process process = Runtime.getRuntime().exec(cmdRename);
                    process.waitFor();
                    if (process.exitValue() == 0) {
                        reply = FTPReply.REQUEST_250;
                    }
                } catch (IOException e) {
                    LOGGER.error("IOException "+e.getMessage());
                } catch (InterruptedException e) {
                    LOGGER.error("InterruptedException "+e.getMessage());
                } finally {
                    client.setFileToRename(null);
                }
            }

        }
        client.send(reply);
        return true;
    }
}
