package car.tp1.process;

import java.io.IOException;
import java.net.Socket;

import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;
import car.tp1.core.FTPReply;
import car.tp1.utils.Logger;

/**
 * Cette classe permet de se connecter en mode actif
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class ProcessPORT implements ProcessRequest {

	public static final String PROCESS_NAME = "PORT";

	public static final Logger LOGGER = new Logger(ProcessPORT.class.getName());

	@Override
	/**
	 * se connecter en mode actif
	 */
	public boolean process(FTPArguments arguments, FTPClient client) {
		String[] command = arguments.getFirstArguments().split(",");
		String ip = command[0]+"."+command[1]+"."+command[2]+"."+command[3];
		int port = Integer.parseInt(command[4])*256+Integer.parseInt(command[5]);
		client.setPort(port);
		client.setIp(ip);
		client.setPassiveMode(false);
		LOGGER.info(port +" "+ip);
		try {
			client.setSocket(new Socket(ip, port));
		} catch (IOException e) {
			LOGGER.error("IOException "+e.getMessage());
			client.send(FTPReply.REQUEST_550);

		}
		client.send(new FTPReply(200, "Okay"));
		return true;
	}
}
