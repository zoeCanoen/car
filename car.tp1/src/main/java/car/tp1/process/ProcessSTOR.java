package car.tp1.process;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;
import car.tp1.core.FTPReply;
import car.tp1.utils.Logger;

/**
 * Cette classe permet d'upload des fichiers
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class ProcessSTOR implements ProcessRequest {

	public static final String PROCESS_NAME = "STOR";

	public static final Logger LOGGER = new Logger(ProcessSTOR.class.getName());

	@Override
	/**
	 * upload des fichiers
	 */
	public boolean process(FTPArguments arguments, FTPClient client) {
		String filename = arguments.getAllArguments();
		String path = client.getCurrentPath();
		LOGGER.info(PROCESS_NAME + " " +  filename);

		OutputStream os;
		Socket socket = null;
		try {

			if (client.getPassiveMode()) {
				LOGGER.debug("passive mode");
				socket = client.getServerSocket().accept();
				os = socket.getOutputStream();
			} else {
				LOGGER.debug("active mode");
				os = client.getSocket().getOutputStream();
			}
			DataOutputStream dos = new DataOutputStream(os);
			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String s = reader.readLine();
			List<String> lines = new ArrayList<>();
			while (s!=null) {
				lines.add(s);
				s = reader.readLine();
			}

			Path newPath = Files.createFile(Paths.get(path, filename));
			Charset charset = Charset.forName("UTF-8");

			Files.write(newPath, lines, charset, StandardOpenOption.APPEND);

            client.send(FTPReply.REQUEST_226);

            if (client.getPassiveMode()) {
            	client.getServerSocket().close();
                socket.close();
            } else {
            	client.getSocket().close();
            }
		} catch (IOException e) {
			LOGGER.error("IOException " + e.getMessage());
			client.send(new FTPReply(550, "Request not executed"));
		}

		LOGGER.debug("End of "+PROCESS_NAME);
		return true;	}
}