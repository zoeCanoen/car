package car.tp1.process;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;
import car.tp1.core.FTPReply;
import car.tp1.utils.Logger;

/**
 * Cette classe permet de creer un dossier
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class ProcessMKD implements ProcessRequest {

	public static final Logger LOGGER = new Logger("ProcessMKD");
	public static final String PROCESS_NAME = "MKD";

	/**
	 * creer un repertoire
	 * @param dirPath
	 * @return
	 */
	public boolean createDir(String dirPath) {
		// Check If Directory Already Exists Or Not?
		Path dirPathObj = Paths.get(dirPath);
		boolean dirExists = Files.exists(dirPathObj);
		if(dirExists) {
			LOGGER.error("Directory Already exists");
		} else {
			try {
				// Creating The New Directory Structure
				Files.createDirectories(dirPathObj);
				LOGGER.info("New directory successfully created");
				return true;
			} catch (IOException e) {
				LOGGER.error("Problem Occured While Creating The Directory Structure= " + e.getMessage());
			}
		}
		return false;

	}

	@Override
	/**
	 * permet de creer un dossier
	 */
	public boolean process(FTPArguments arguments, FTPClient client) {
		String stringData = arguments.getAllArguments();
		FTPReply reply = FTPReply.REQUEST_257;
		reply.replacePathname(stringData);
		LOGGER.info(client.getCurrentPath()+"/"+stringData);
		if (createDir(client.getCurrentPath()+"/"+stringData))
			client.send(reply);
		else
			client.send(FTPReply.REQUEST_550);
		return true;
	}
}
