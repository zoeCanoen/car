package car.tp1.process;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.Random;

import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;
import car.tp1.core.FTPReply;
import car.tp1.utils.Logger;

/**
 * Cette classe permet de se connecter en mode passif
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class ProcessPASV implements ProcessRequest {

    public static final String PROCESS_NAME = "PASV";

    public static final Logger LOGGER = new Logger(ProcessPASV.class.getName());

    @Override
    /**
     * se connecter en mode passif
     */
    public boolean process(FTPArguments arguments, FTPClient client) {
        InetAddress inetAddress = null;
        try {
            inetAddress = InetAddress.getLocalHost();

        } catch (UnknownHostException e) {
            LOGGER.error("UnknownHostException " + e.getMessage());
            client.send(new FTPReply(501, "Syntaxe error"));
            return false;
        }
        int port1 = client.getPort() >> 8;
        int port2 = client.getPort() & 0xFF;
        String s = inetAddress.toString();
        client.setIp(s.substring(s.indexOf("/") + 1)); // chez moi j'avais une IP MSI/10.135.10.146 avec windows
        client.setPassiveMode(true);
        LOGGER.info("Entering Passive Mode (" + client.getIp().replaceAll("\\.", ",") + "," + port1 + "," + port2 + ")");

        try {
            client.setServerSocket(new ServerSocket(client.getPort()));
        } catch (IOException e) {
            LOGGER.error("IOException "+e.getMessage());
        	if  (e.getMessage().contains("IOException Address already in use (Bind failed)")){
        		Random r = new Random();
        		int port = 8081 + r.nextInt(15000 - 8081);
        		client.setPort(port);
        		client.send(FTPReply.REQUEST_550);
        		return true;
        	}
        }
        client.send(new FTPReply(227, "Entering Passive Mode (" + client.getIp().replaceAll("\\.", ",") + "," + port1 + "," + port2 + ")"));
        LOGGER.debug("Entering Passive Mode Ok");

        return true;
    }
}
