package car.tp1.process;

import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;
import car.tp1.core.FTPDatabase;
import car.tp1.core.FTPReply;
import car.tp1.utils.Logger;

/**
 * Cette classe permet de verifier le mot de passe
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class ProcessPASS implements ProcessRequest {

    public static final String PROCESS_NAME = "PASS";

    public static final Logger LOGGER = new Logger(ProcessPASS.class.getName());


    @Override
    /**
     * verifier le mot de passe
     */
    public boolean process(FTPArguments arguments, FTPClient client) {
    	String username = client.getUsername();
        String password = arguments.getFirstArguments();

        if (password != null && FTPDatabase.INSTANCE.verifyPasswordUser(username, password)) {
            client.setPassword(password);
            client.send(FTPReply.REQUEST_230);
            LOGGER.info("Successful authentification");
        } else {
            client.send(FTPReply.REQUEST_430);
            LOGGER.info("Incorrect password");
        }
        return true;
    }
}