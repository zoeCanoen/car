package car.tp1.process;

import java.io.File;

import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;
import car.tp1.core.FTPReply;
import car.tp1.utils.Logger;
import car.tp1.utils.PathBuilder;

/**
 * Cette classe permet de  se deplacer dans un repertoire distant
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class ProcessCWD implements ProcessRequest {

	public static final String PROCESS_NAME = "CWD";

	public static final Logger LOGGER = new Logger(ProcessCWD.class.getName());

	@Override
	/**
	 * se deplacer dans un repertoire distant
	 */
	public boolean process(FTPArguments arguments, FTPClient client) {
		String path = arguments.getAllArguments();
		LOGGER.debug(path);
		path = new PathBuilder().arrayToPath(path, client.getCurrentPath());
		LOGGER.info(PROCESS_NAME+" : "+path);
		final File f = new File(path);

		String newPath = f.getAbsolutePath().replace("\\", "/");
		client.setCurrentPath(newPath);
		LOGGER.debug(newPath);
		client.send(new FTPReply(250, newPath));
		return true;

	}

}

