package car.tp1.process;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;

import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;
import car.tp1.core.FTPReply;
import car.tp1.utils.Logger;

/**
 * Cette classe permet de telecharger un fichier
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class ProcessRETR implements ProcessRequest {

    public static final String PROCESS_NAME = "RETR";

    public static final Logger LOGGER = new Logger(ProcessRETR.class.getName());

    @Override
    /**
     * telecharger un fichier
     */
    public boolean process(FTPArguments arguments, FTPClient client) {
    	String filename = arguments.getAllArguments();
		String path = client.getCurrentPath() + "/" + filename; 
		LOGGER.info(PROCESS_NAME  + path);

		OutputStream os;
		Socket socket = null;
		try {
			client.send(new FTPReply(150, "Upload"));
			if (client.getPassiveMode()) {
				LOGGER.debug("passive mode");
				socket = client.getServerSocket().accept();
				os = socket.getOutputStream();
			} else {
				LOGGER.debug("active mode");
				os = client.getSocket().getOutputStream();
			}
			DataOutputStream dos = new DataOutputStream(os);
			String s = null;
			BufferedReader reader = Files.newBufferedReader(Paths.get(path));
			while ((s = reader.readLine()) != null) {
				LOGGER.debug(s);
				dos.writeBytes(s + "\r\n");
			}
            client.send(FTPReply.REQUEST_226);

            if (client.getPassiveMode()) {
            	client.getServerSocket().close();
                socket.close();
            } else {
            	client.getSocket().close();
            }
		} catch (IOException e) {
			LOGGER.error("IOException " + e.getMessage());
			client.send(new FTPReply(550, "Request not executed"));
		}

		LOGGER.debug("End of "+PROCESS_NAME);
		return true;
    }
}