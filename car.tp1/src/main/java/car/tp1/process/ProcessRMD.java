package car.tp1.process;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;
import car.tp1.core.FTPReply;
import car.tp1.utils.Logger;

/**
 * Cette classe permet de supprimer un repertoire
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class ProcessRMD implements ProcessRequest {

	public static final Logger LOGGER = new Logger("ProcessRDM");
	public static final String PROCESS_NAME = "RMD";

	@Override
	/**
	 * supprimer un repertoire
	 */
	public boolean process(FTPArguments arguments, FTPClient client) {
		String stringData = arguments.getAllArguments();	
		String path = client.getCurrentPath()+"/"+stringData;
		FTPReply reply = FTPReply.REQUEST_257;
		reply.replacePathname(path);
		LOGGER.info("RMD : "+path);
		Path rootPath = Paths.get(path);
		try {
			Files.walk(rootPath)
			.sorted(Comparator.reverseOrder())
			.map(Path::toFile)
			.forEach(File::delete);
			client.send(reply);
		} catch (IOException e) {
			LOGGER.error("Can't remove this repository.");
			client.send(FTPReply.REQUEST_550);
		}


		return true;
	}
}
