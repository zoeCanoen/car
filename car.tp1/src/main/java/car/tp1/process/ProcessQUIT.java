package car.tp1.process;

import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;
import car.tp1.core.FTPReply;
import car.tp1.utils.Logger;

/**
 * Cette classe permet de quitter le serveur ftp
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class ProcessQUIT implements ProcessRequest {

    public static final String PROCESS_NAME = "QUIT";

    public static final Logger LOGGER = new Logger(ProcessQUIT.class.getName());

    @Override
    /**
     * quitter le serveur ftp
     */
    public boolean process(FTPArguments arguments, FTPClient client) {
    	LOGGER.info(PROCESS_NAME);
    	client.send(FTPReply.REQUEST_221);
        return false;
    }
}