package car.tp1.process;

import java.io.*;
import java.net.Socket;

import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;
import car.tp1.core.FTPReply;
import car.tp1.utils.Logger;

/**
 * Cette classe permet de d'aficher le contenu du dossier
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class ProcessLIST implements ProcessRequest {

    public static final String PROCESS_NAME = "LIST";

    public static final Logger LOGGER = new Logger(ProcessLIST.class.getName());

    /**
     * aficher le contenu du dossier
     */
    public boolean process(FTPArguments arguments, FTPClient client) {
        LOGGER.info(PROCESS_NAME+" : " + client.getCurrentPath());
        OutputStream os;
        DataOutputStream dos = null;
        Socket socket = null;
        // init necessary flows
        try {
            client.send(new FTPReply(150, "List of file :"));
            if (client.getPassiveMode()) {
            	LOGGER.debug("passive mode");
            	socket = client.getServerSocket().accept();
            	os = socket.getOutputStream(); // 2e ServerSocket dans le cas passif (on donne une ip et un port au client => PASV)
            } else {
            	LOGGER.debug("active mode");
                os = client.getSocket().getOutputStream(); // socket de data donn� par PORT (mode actif)
            }
            dos = new DataOutputStream(os); // on cr�� un DataOutputStream pour �crire dans le socket

            // Launch the external PROCESS_NAME
            LOGGER.debug("ls -l " + client.getCurrentPath());
            Runtime r = Runtime.getRuntime();
            Process p = r.exec("ls -l " + client.getCurrentPath());
			p.waitFor();

            // create an appropriate reader to capture outputs of the PROCESS_NAME
            BufferedReader bf = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String s = null;
            while ((s = bf.readLine()) != null) {
            	LOGGER.debug(s);
                dos.writeBytes(s + "\r\n"); // tu �cris la ligne en question (autant de fois qu'il y a de fichiers comme c'est une boucle)
            }
            // il faut fermer le socket de data pour que le client sache qu'on a fini de tranf�rer les donn�es, IMPERATIVEMENT APRES LE 226
            
            client.send(FTPReply.REQUEST_226); // sur le socket de commande on �crit 226

            if (client.getPassiveMode()) {
            	client.getServerSocket().close();
                socket.close();
            } else {
            	client.getSocket().close();
            }
            

        } catch (IOException e) {
            LOGGER.error("IOException " + e.getMessage());
			client.send(FTPReply.REQUEST_550);
        }
        catch (InterruptedException e) {
            LOGGER.error("InterruptedException " + e.getMessage());
			client.send(FTPReply.REQUEST_550);
		}
        LOGGER.debug("End of LIST");
        return true;
    }

}