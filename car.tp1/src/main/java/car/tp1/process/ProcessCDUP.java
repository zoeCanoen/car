package car.tp1.process;

import java.io.File;

import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;
import car.tp1.core.FTPReply;
import car.tp1.utils.Logger;

/**
 * Cette classe permet de remonter dans le repertoire parent
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class ProcessCDUP implements ProcessRequest {

    public static final String PROCESS_NAME = "CDUP";

    public static final Logger LOGGER = new Logger(ProcessCDUP.class.getName());
	
    @Override
    /**
     * remonter dans le repertoire parent
     */
    public boolean process(FTPArguments arguments, FTPClient client) {
    	LOGGER.info(client.getCurrentPath() + "/..");
        final File f = new File(client.getCurrentPath() + "/..");
        if (!f.exists() || !f.isDirectory()) {
        	LOGGER.error("No such file or directory "+ client.getCurrentPath() + "/..");
        	client.send(new FTPReply(550, "No such file or directory "+ client.getCurrentPath() + "/.."));
        }
        String newPath = f.getAbsolutePath().replace("\\", "/");
        client.setCurrentPath(newPath);
        LOGGER.debug(newPath);
        client.send(new FTPReply(250, newPath));
    return true;
    }
    

}

