package car.tp1.process;


import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;
import car.tp1.core.FTPReply;
import car.tp1.utils.Logger;

/**
 * Cette classe permet de sauvegarder le path a renommer
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class ProcessRNFR implements ProcessRequest {

    public static final String PROCESS_NAME = "RNFR";

    public static final Logger LOGGER = new Logger(ProcessRNFR.class.getName());

    @Override
    /**
     * sauvegarder le path a renommer
     */
    public boolean process(FTPArguments arguments, FTPClient client) {
        String argument = arguments.getAllArguments();
        LOGGER.info(argument);
        FTPReply reply = FTPReply.REQUEST_501;

        if (arguments.isNotEmpty()) {
            client.setFileToRename(argument);
            reply = FTPReply.REQUEST_350;
        }

        client.send(reply);
        return true;
    }
}
