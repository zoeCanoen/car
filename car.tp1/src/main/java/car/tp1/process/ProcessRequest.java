package car.tp1.process;

/**
 * Interface des processus utilisateur
 * @author Zoe Canoen et Anthony Slimani
 *
 */
import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;

public interface ProcessRequest {

    boolean process(FTPArguments arguments, FTPClient client);


}
