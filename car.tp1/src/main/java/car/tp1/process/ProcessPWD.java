package car.tp1.process;

import car.tp1.core.FTPArguments;
import car.tp1.core.FTPClient;
import car.tp1.core.FTPReply;
import car.tp1.utils.Logger;

/**
 * Cette classe permet de mettre a jour le repertoire courant
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class ProcessPWD implements ProcessRequest {

    public static final String PROCESS_NAME = "PWD";

    public static final Logger LOGGER = new Logger(ProcessPWD.class.getName());

    @Override
    /**
     * mettre a jour le repertoire courant
     */
    public boolean process(FTPArguments arguments, FTPClient client) {
    	client.send(new FTPReply(257, client.getCurrentPath()+" is your current location"));
        LOGGER.info("Origin path : "+client.getCurrentPath());
        return true;

    }
}
