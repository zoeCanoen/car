package car.tp1;

import car.tp1.core.FTPClient;
import car.tp1.core.FTPServer;
import car.tp1.utils.Logger;
import car.tp1.utils.PropertiesReader;

/**
 * Lance l'execution du serveur
 *
 * @author canoen
 */
public class Main {

	public static final Logger LOGGER = new Logger("Main");

    public static void main(String[] args) {
    	LOGGER.info("Run Server");
        FTPClient client = new FTPClient(PropertiesReader.INSTANCE.getOriginPath());

        FTPServer server = new FTPServer(client, PropertiesReader.INSTANCE.getPort());
        
        while (true) {
            if (server.connect()) {
                server.processing();
            }
        }
        

    }
}
