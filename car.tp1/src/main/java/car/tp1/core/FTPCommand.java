package car.tp1.core;

/**
 * Cette classe permet de separer la commande du client en nom/argument
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class FTPCommand {

    public static final String SEPARATOR_COMMAND = " ";

    private String name;
    private FTPArguments arguments;

    public FTPCommand(String command) {
        this.name = extractName(command);
        this.arguments = extractArguments(command);
    }

    private String extractName(String command) {
        String[] commandSplit = command.split(SEPARATOR_COMMAND);
        if (commandSplit.length > 0) {
            return commandSplit[0];
        }
        return "";
    }

    private FTPArguments extractArguments(String command) {
        int indexFirstSeparator = command.indexOf(SEPARATOR_COMMAND);
        return new FTPArguments(command.substring(indexFirstSeparator + 1));
    }

    public String getName() {
        return name;
    }

    public FTPArguments getArguments() {
        return arguments;
    }

    @Override
    public String toString() {
        return name + SEPARATOR_COMMAND + arguments.toString();
    }
}
