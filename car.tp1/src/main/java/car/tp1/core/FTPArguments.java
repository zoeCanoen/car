package car.tp1.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Cette classe permet de separer les arguments des commandes du client
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class FTPArguments {

    public static final String SEPARATOR_ARGUMENTS = " ";

    private String allArguments;
    private List<String> arguments;

    public FTPArguments(String arguments) {
        this.allArguments = arguments;
        this.arguments = extractArguments();
    }

    private List<String> extractArguments() {
        String[] argumentsSplit = allArguments.split(SEPARATOR_ARGUMENTS);
        if (argumentsSplit.length > 0) {
            return Arrays.asList(argumentsSplit);
        }
        return new ArrayList<>();
    }

    public String getFirstArguments() {
        if (!arguments.isEmpty()) {
            return arguments.get(0);
        }
        return null;
    }

    public boolean isNotEmpty() {
        return allArguments != null && !allArguments.trim().isEmpty();
    }

    public boolean onlyOneArgument() {
        return arguments.size() == 1 && isNotEmpty();
    }

    public String getAllArguments() {
        return allArguments;
    }

    public void setAllArguments(String allArguments) {
        this.allArguments = allArguments;
    }

    public List<String> getArguments() {
        return arguments;
    }

    public void setArguments(List<String> arguments) {
        this.arguments = arguments;
    }
}
