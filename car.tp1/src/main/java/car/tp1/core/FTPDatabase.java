package car.tp1.core;

import java.util.HashMap;
import java.util.Map;

/**
 * Contient les utilisateurs, login et mot de passe.
 * @author Zoe Canoen et Anthony Slimani
**/
public class FTPDatabase {

    public static final FTPDatabase INSTANCE = new FTPDatabase();

    private final Map<String, String> users;

    private FTPDatabase() {
        this.users = new HashMap<>();
        this.users.put("canoen", "canoen");
        this.users.put("slimani", "slimani");
    }

    public boolean usernameExist(String username) {
        return username != null && users.get(username) != null;
    }

    public boolean verifyPasswordUser(String username, String password) {
        return username != null && users.get(username).equals(password);
    }

}
