package car.tp1.core;

import java.util.HashMap;
import java.util.Map;

import car.tp1.exception.UnimplementedCommandException;
import car.tp1.process.ProcessCDUP;
import car.tp1.process.ProcessCWD;
import car.tp1.process.ProcessDELE;
import car.tp1.process.ProcessLIST;
import car.tp1.process.ProcessMKD;
import car.tp1.process.ProcessPASS;
import car.tp1.process.ProcessPASV;
import car.tp1.process.ProcessPORT;
import car.tp1.process.ProcessPWD;
import car.tp1.process.ProcessQUIT;
import car.tp1.process.ProcessRETR;
import car.tp1.process.ProcessRMD;
import car.tp1.process.ProcessRNFR;
import car.tp1.process.ProcessRNTO;
import car.tp1.process.ProcessRequest;
import car.tp1.process.ProcessSTOR;
import car.tp1.process.ProcessTYPE;
import car.tp1.process.ProcessUSER;

/**
 * Cette classe permet de stocker tous les processus utilisateur
 *
 * @author Zoe CANOEN
 * @author Anthony SLIMANI
 */
class FTPRequest {

    private Map<String, ProcessRequest> listCommands = new HashMap<>();

    public FTPRequest() {
        constructCommands();
    }

    public boolean executeCommand(String command, FTPArguments arguments, FTPClient client) throws UnimplementedCommandException {
        ProcessRequest processRequest = listCommands.get(command.toUpperCase());
        if (processRequest != null && arguments != null) {
            return processRequest.process(arguments, client);
        } else {
            throw new UnimplementedCommandException();
        }
    }

    /**
     * Creation de la liste des traitemnts possibles
     */
    private void constructCommands() {
        listCommands.put(ProcessCDUP.PROCESS_NAME, new ProcessCDUP());
        listCommands.put(ProcessCWD.PROCESS_NAME, new ProcessCWD());
        listCommands.put(ProcessDELE.PROCESS_NAME, new ProcessDELE());
        listCommands.put(ProcessQUIT.PROCESS_NAME, new ProcessQUIT());
        listCommands.put(ProcessLIST.PROCESS_NAME, new ProcessLIST());
        listCommands.put(ProcessMKD.PROCESS_NAME, new ProcessMKD());
        listCommands.put(ProcessPASS.PROCESS_NAME, new ProcessPASS());
        listCommands.put(ProcessPASV.PROCESS_NAME, new ProcessPASV());
        listCommands.put(ProcessPORT.PROCESS_NAME, new ProcessPORT());
        listCommands.put(ProcessPWD.PROCESS_NAME, new ProcessPWD());
        listCommands.put(ProcessSTOR.PROCESS_NAME, new ProcessSTOR());
        listCommands.put(ProcessTYPE.PROCESS_NAME, new ProcessTYPE());
        listCommands.put(ProcessUSER.PROCESS_NAME, new ProcessUSER());
        listCommands.put(ProcessRMD.PROCESS_NAME, new ProcessRMD());
        listCommands.put(ProcessRETR.PROCESS_NAME, new ProcessRETR());
        listCommands.put(ProcessRNFR.PROCESS_NAME, new ProcessRNFR());
        listCommands.put(ProcessRNTO.PROCESS_NAME, new ProcessRNTO());
    }
}








