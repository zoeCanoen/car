package car.tp1.core;

/**
 * Reponse du serveur au client avec un code de status et un message de description
 *
 * @author Zoe CANOEN
 * @author Anthony SLIMANI
 */
public class FTPReply {

    public static final String PATHNAME = "PATHNAME";

    //Série de réponses 2xx
    public static final FTPReply REQUEST_200 = new FTPReply(200, "FTPCommand okay");
    public static final FTPReply REQUEST_221 = new FTPReply(221, "Disconnection");
    public static final FTPReply REQUEST_226 = new FTPReply(226, "Closing data connection");
    public static final FTPReply REQUEST_230 = new FTPReply(230, "Successful authentication");
    public static final FTPReply REQUEST_250 = new FTPReply(250, "Requested file action okay, completed");
    public static final FTPReply REQUEST_257 = new FTPReply(257, "\"" + PATHNAME + "\" created");

    //Série de réponses 3xx
    public static final FTPReply REQUEST_331 = new FTPReply(331, "User name okay, need password");
    public static final FTPReply REQUEST_350 = new FTPReply(350, "Requested file action pending further information");

    //Série de réponses 4xx
    public static final FTPReply REQUEST_430 = new FTPReply(430, "Incorrect username or password");

    //Série de réponses 5xx
    public static final FTPReply REQUEST_501 = new FTPReply(501, "Syntax error in parameters or argument");
    public static final FTPReply REQUEST_502 = new FTPReply(502, "Unimplemented command");
    public static final FTPReply REQUEST_503 = new FTPReply(503, "Bad sequence of commands");
    public static final FTPReply REQUEST_504 = new FTPReply(504, "FTPCommand not implemented for that parameter");
    public static final FTPReply REQUEST_550= new FTPReply(550, "Request not executed");



    private int statusCode;
    private String message;

    public FTPReply(int statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    public String getFormatReponse() {
        return statusCode + " " + message + "\r\n";
    }

    public String getMessage() {
        return message;
    }

    public void replacePathname(String pathName) {
        this.message = message.replace(PATHNAME, pathName);
    }
}
