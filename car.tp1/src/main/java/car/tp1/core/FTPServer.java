package car.tp1.core;

import car.tp1.exception.UnimplementedCommandException;
import car.tp1.utils.Logger;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 * Le serveur gere toutes les commandes utilisateurs et le executent.
 * @author Zoe CANOEN
 * @author Anthony SLIMANI
 */
public class FTPServer {

	public static final Logger LOGGER = new Logger("FTPServer");

    private int port;

	private FTPClient client;

	public static String MODE;	
	
    private static final int TIME_OUT = 10000000; //en millisecondes
    private static final String MSG_WAIT_CLIENT = "En attente du client...";
    private static final String MSG_CONNECTION_ESTABLISHED = "Connexion etablie.";

    private ServerSocket serverSocket;
    private Socket socket;

	private DataOutputStream writer;


    public FTPServer(FTPClient client, int port) {
        this.client = client;
        this.setPort(port);
	}

	/**
     * Creation des sockets serveur avec un timeout
     *
     * @return reussite ou echec de la creation des sockets
     */
    public boolean connect() {
        try {
            this.serverSocket = new ServerSocket(port);
            LOGGER.info(MSG_WAIT_CLIENT);
            this.socket = this.serverSocket.accept();
            this.socket.setSoTimeout(TIME_OUT);
            writer = new DataOutputStream(socket.getOutputStream());
            client.setOutputStream(writer);
            LOGGER.info(MSG_CONNECTION_ESTABLISHED);
            client.send(FTPReply.REQUEST_200);
            return true;
        } catch (IOException e) {
        	LOGGER.error("connect() - IOException " + e.getMessage());
            return false;
        }
    }

    /**
     * Lecture des instructions envoyes et envoie de reponses
     */
    public void processing() {
        boolean connection = true;
        FTPRequest ftpRequest = new FTPRequest();
        String error="";
        try {
            while (connection) {
                try {
                    String message = receive();
                    if (message == null) {
                        connection = false;
                    } else {
                        FTPCommand command = new FTPCommand(message);
                        error = command.getName() + " was not implemented";
                        connection = ftpRequest.executeCommand(command.getName(), command.getArguments(), client);
                    }
                } catch (UnimplementedCommandException e) {
                    client.send(FTPReply.REQUEST_502);
                    LOGGER.error(error);
                }
            }
        } catch (SocketTimeoutException s) {
        	LOGGER.error("Socket timed out!");
        } catch (IOException e) {
        	LOGGER.error("IOException " + e.getMessage());
        }

        closure();
    }

    /**
     * Fermeture des sockets serveur
     */
    public void closure() {
        try {
            socket.close();
            serverSocket.close();
        } catch (IOException e) {
        	LOGGER.error("IOException " + e.getMessage());
        }
    }


    /**
     * Reception d'une ligne sur la flux d'entree du socket serveur
     *
     * @return ligne lue
     * @throws IOException dans le cas ou l'InputStream est ferme, ou que le socket n'est pas connecte ou ferme
     */
    public String receive() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        return reader.readLine();
    }

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
}