package car.tp1.core;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import car.tp1.utils.Logger;

/**
 * Cette classe permet de stocker toutes les informations du client
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class FTPClient extends Thread {

	public static final Logger LOGGER = new Logger("FTPClient");

	private int port = 10090;
	private ServerSocket serverSocket;
	private Socket socket;
	private String ip;

	private String username;
	private String password;
	private String currentPath;
	private String originPath;
	private String fileToRename;
	private boolean passiveMode;
	private DataOutputStream writer;


	public FTPClient(String originPath) {
		this.currentPath = originPath;
		this.originPath = originPath;
	}

    public void send(FTPReply reponse) {
        try {
            writer.writeBytes(reponse.getFormatReponse());
        } catch (IOException e) {
            LOGGER.error("IOException " + e.getMessage() + "\nCan't send : " + reponse.getFormatReponse());
        }
    }
    
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCurrentPath() {
		return currentPath;
	}

	public void setCurrentPath(String currentPath) {
		this.currentPath = currentPath;
	}

	public String getOriginPath() {
		return originPath;
	}

	public void setOriginPath(String originPath) {
		this.originPath = originPath;
	}

	public ServerSocket getServerSocket() {
		return serverSocket;
	}

	public boolean getPassiveMode() {
		return passiveMode;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port=port;
	}

	public void setIp(String ip) {
		this.ip=ip;
	}

	public String getIp() {
		return ip;
	}

	public Socket getSocket() {
		return socket;
	}

	public void setPassiveMode(boolean b) {
		passiveMode=b;
	}

	public void setServerSocket(ServerSocket serverSocket) {
		this.serverSocket=serverSocket;
	}
	
	public void setSocket(Socket socket) {
		this.socket=socket;
	}

	public String getFileToRename() {
		return fileToRename;
	}

	public void setFileToRename(String fileToRename) {
		this.fileToRename = fileToRename;
	}

	public void setOutputStream(DataOutputStream writer) {
		this.writer=writer;
	}
}
