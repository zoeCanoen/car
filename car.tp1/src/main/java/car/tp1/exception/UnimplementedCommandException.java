package car.tp1.exception;

public class UnimplementedCommandException extends Exception {

    private static final long serialVersionUID = 1L;

    public UnimplementedCommandException() {
        super();
    }
}
