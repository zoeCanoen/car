package car.tp1.utils;

/**
 * Cette classe permet de creer le bon path
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class PathBuilder {

	  public String arrayToPath(String path, String clientPath) {
	    if (path.length() > 0 && path.charAt(0) == '/') {
	      return path;
	    }
	    if (clientPath.charAt(clientPath.length()-1)=='/')
	    	return clientPath + path;
	    return clientPath + "/" + path;
	  }
}
