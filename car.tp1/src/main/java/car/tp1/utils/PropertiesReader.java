package car.tp1.utils;

import java.util.ResourceBundle;

/**
 * Cette classe permet de lire un fichier properties
 * @author Zoe Canoen et Anthony Slimani
 *
 */
public class PropertiesReader {
	
	public static final PropertiesReader INSTANCE = new PropertiesReader("app");
	private ResourceBundle rb;
	
	private PropertiesReader(String filename) {
	    rb = ResourceBundle.getBundle(filename);
	}

    public int getPort() {
    	return Integer.parseInt(rb.getString("port"));
    }
    
    public String getOriginPath() {
    	return rb.getString("originPath");
    }

    public String getLogLevel() {
    	return rb.getString("logLevel");
    }
}
